"""
This file contains all the utility functions including the required functions for S3,
merging files from a folder into csv, and merging experiments together.
"""

import os
import io
import pandas as pd
import boto3
from os import listdir
import sys

# S3 Credentials
bucket_name = 'BUCKET_NAME'

# Modify these variables with user's respective AWS keys
aws_id = 'AWS_KEY'
aws_secret = 'AWS_SECRET_KEY'


def read_from_s3(filename, is_csv):
    """function can help us to read file from S3 bucket
    Args:
        filename (String): the key path to the file in S3
        is_csv (boolean): if the file is boolean or not
    Returns:
        df (DataFrame) : file converted to dataframe read from S3
    """
    client = boto3.client('s3', aws_access_key_id=aws_id,
                          aws_secret_access_key=aws_secret)

    object_key = filename
    csv_obj = client.get_object(Bucket=bucket_name, Key=object_key)

    if is_csv:
        df = pd.read_csv(io.BytesIO(csv_obj['Body'].read()))
    else:
        df = pd.read_stata(io.BytesIO(csv_obj['Body'].read()))
    return df


def write_to_s3(file, file_key):
    """function can help us write a csv file to S3 bucket
    Args:
        file (DataFrame): file to be written
        file_key (String): name of the file (the path at which file is to be stored in S3)
    """
    client = boto3.client('s3', aws_access_key_id=aws_id,
                          aws_secret_access_key=aws_secret)

    csv_buf = io.StringIO()
    file.to_csv(csv_buf, index=None)
    csv_buf.seek(0)
    client.put_object(Bucket=bucket_name, Body=csv_buf.getvalue(), Key=file_key)
    
    
def combine_files(folder_name):
    """function to merge results of all blocks from an experiment into a single csv file.
    Args:
        folder_name(string): name of the folder where results of blocks (from an experiment) exists.
    Returns:
        combined_csv (DataFrame): DataFrame representing the combined csv file
    """
    #get all files in the folder
    all_filenames = [f for f in listdir(folder_name)]

    #combine all files in the list
    combined_csv = pd.concat([pd.read_csv(folder_name+"\\"+f)\
                          for f in all_filenames], ignore_index = True,\
                          sort= False)
    print(combined_csv.shape)

    #export to csv
    combined_csv.to_csv( "combined_"+folder_name+".csv", index=False)
    return combined_csv
    
    
def merge_experiments(df):
    """This function helps us to combine all the experiment dataframes into one dataframe
    which has flags in each row for each experiment.
    Args:
        df (List of DataFrames): needs to be merged
    Returns:
        merged (DataFrame) : a single dataframe containing all experiment results
    """
    flags = ['match_flag{s}'.format(s=i+1) for i in range(len(df))]
    merged = pd.DataFrame(columns=['examiner_id', 'pseudo_id','dfB.match[, names.dfB]'])
    
    for i in range(len(df)):
        df[i] = (df[i])[['examiner_id', 'pseudo_id','dfB.match[, names.dfB]', 'posterior']]
        for j in range(len(flags)):
            if j == i:
                (df[i])[flags[j]] = 1
            else:
                (df[i])[flags[j]] = 0

        merged = pd.merge(left=merged, right=df[i], how='outer', left_on=['pseudo_id', 'examiner_id', 'dfB.match[, names.dfB]'], right_on=['pseudo_id', 'examiner_id', 'dfB.match[, names.dfB]'])
        if i!=0:
            for k in range(len(df)):
                if k==i:
                    merged = merged.replace({'match_flag{s}_y'.format(s=k+1): {np.nan:0.0}})
                    merged = merged.drop(columns = ['match_flag{s}_x'.format(s=k+1)])
                    merged = merged.rename(columns={'match_flag{s}_y'.format(s=k+1): 'match_flag{s}'.format(s=k+1)})
                else:
                    merged = merged.replace({'match_flag{s}_x'.format(s=k+1): {np.nan:0.0}})
                    merged = merged.drop(columns = ['match_flag{s}_y'.format(s=k+1)])
                    merged = merged.rename(columns={'match_flag{s}_x'.format(s=k+1): 'match_flag{s}'.format(s=k+1)})
                                
        merged = merged.rename(columns={'posterior': 'posterior_{s}'.format(s=(i+1))})
    
    merged = merged.rename(columns={'dfB.match[, names.dfB]': 'voter_id'})
    return merged